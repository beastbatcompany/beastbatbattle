﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hud : MonoBehaviour
{

    public PlayerController personaje;
    public GameObject BarraVidas;
    private Animator anim;
    public const string ESTADO_VIDAS = "Vidas";

	// Use this for initialization
	void Start ()
    {
        anim = BarraVidas.GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void Update ()

    {
        anim.SetInteger("Vidas", personaje.Vidas);
	}
}
