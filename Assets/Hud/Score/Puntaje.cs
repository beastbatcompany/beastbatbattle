﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puntaje : MonoBehaviour { 

    public PlayerController personaje;
    public GameObject centenas, decenas, unidades;
    private Animator ce, de, un;
    private string[] Estados = { "Estado_00", "Estado_01", "Estado_02", "Estado_03", "Estado_04", "Estado_05", "Estado_06", "Estado_07", "Estado_08", "Estado_09" };
    

	// Use this for initialization
	void Start () {
        ce = centenas.GetComponent<Animator>();
        de = decenas.GetComponent<Animator>();
        un = unidades.GetComponent<Animator>();

        ce.Play(Estados[0]);
        de.Play(Estados[1]);
        un.Play(Estados[2]);
    	
	}
	
	// Update is called once per frame
	void Update (){
        ActualizarPuntaje(personaje.Puntos);
    }
    public void ActualizarPuntaje (int numero)
    {
        int unidades = numero % 10;
        int decenas = numero % 100 - unidades;
        int centenas = numero % 1000 - decenas;

        //Debug.Log("numero" +  numero + "centenas" + centenas/100 + "decenas " + decenas/10 + unidades);
        decenas = decenas / 10;
        centenas = centenas / 100; 

        if (numero > 9)
        {
            //hay decenas
            de.Play(Estados[decenas]);
        }
        else
        {
            de.Play(Estados[0]);
        }
        if (numero > 99)
        {
            //hay centenas
            ce.Play(Estados[centenas]);
        }
        else
        {
            ce.Play(Estados[0]);
        }
        un.Play(Estados[unidades]);
    }
}
