﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public float altura_salto;
    public float velocidad_movimiento;
    private Rigidbody2D rb;
    private bool tocando_piso;
    private Animator anim;
    public LayerMask capa_piso;
    public float radio_validacion;
    public Transform validador_piso;
    private Vector2 pos_o;
    public const string MONEDA = "Moneda";
    public const string MUERTE = "Muerte";
    public const string PLATAFORMA_MOVIL = "PlataformaMovilVertical";
    public const string PISO = "Piso";
    private int vidas = 8;
    private int puntos = 0;

    //Asigna o retorna vidas

    public int Vidas
    {
        get
        {
            return vidas;
        }

        set
        {
            vidas = value;
        }
    }

    public int Puntos
    {
        get
        {
            return puntos;
        }

        set
        {
            puntos = value;
        }
    }


    // Use this for initialization

    void Start()
    {
        pos_o = this.transform.position;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        anim.SetInteger("Estado", 0);
      
    }

    /*void OnCollisionEnter2D(Collision2D c)
    {
        tocando_piso = c.gameObject.tag.Equals("Piso");  
    }*/

    void FixedUpdate()
    {
        tocando_piso = Physics2D.OverlapCircle(validador_piso.position, radio_validacion, capa_piso);
    }

    // Update is called once per frame
    void Update () {
        if (tocando_piso)
        {
            anim.SetInteger("Estado", 0);
        }

        if (Input.GetKey(KeyCode.Space) && tocando_piso)
        {
            rb.velocity = new Vector2(rb.velocity.x, altura_salto);
            tocando_piso = false;
            anim.SetInteger("Estado", 2);
            
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocidad_movimiento, rb.velocity.y);
            rb.transform.localScale = new Vector2(1, 1);
            anim.SetInteger("Estado", 1);
        }
		if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocidad_movimiento, rb.velocity.y);
            rb.transform.localScale = new Vector2(-1, 1);
            anim.SetInteger("Estado", 1);
        }
        
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag.Equals(MONEDA))
        {
            collider.gameObject.SetActive(false);

            puntos++;
        }
    }

    void OnCollisionEnter2D(Collision2D c)
    {     
        if (c.transform.tag.Equals(PISO))
        {
            transform.parent = c.transform;
        }

        else
        {
            transform.parent = null;
        }
        if (c.gameObject.tag == "Muerte")
        {
            GetComponent<CapsuleCollider2D>().enabled = false;

            if (vidas > 1)
            {
                this.transform.position = pos_o;

                print("colision");
            }

            if (vidas <= 1)
            {
                SceneManager.LoadScene(4);
            }

            vidas -= 1;

            GetComponent<CapsuleCollider2D>().enabled = true;
        }
    }
}
