﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MYAEnemigos : MonoBehaviour
{

    public GameObject Enemigos;
    public Transform Posicion_Inicial;
    public Transform Posicion_Final;
    private Transform Posicion_Siguiente;
    public float velocidad;

    // Use this for initialization
    void Start()
    {
        Posicion_Siguiente = Posicion_Final;

    }

    // Update is called once per frame
    void Update()
    {
        Enemigos.transform.position = Vector2.MoveTowards(Enemigos.transform.position, Posicion_Siguiente.position, Time.deltaTime * velocidad);

        if (Enemigos.transform.position == Posicion_Siguiente.position)
        {
            Posicion_Siguiente = Posicion_Siguiente == Posicion_Final ? Posicion_Inicial : Posicion_Final;
        }
    }
}