﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour {

    public GameObject Plataforma;
    public Transform Posicion_Inicial;
    public Transform Posicion_Final;
    private Transform Posicion_Siguiente;
    public float velocidad;

	// Use this for initialization
	void Start () {
        Posicion_Siguiente = Posicion_Final;
		
	}

    // Update is called once per frame
    void Update() {
        Plataforma.transform.position = Vector2.MoveTowards(Plataforma.transform.position, Posicion_Siguiente.position, Time.deltaTime * velocidad);
        
        if(Plataforma.transform.position == Posicion_Siguiente.position)
        {
            Posicion_Siguiente = Posicion_Siguiente == Posicion_Final ? Posicion_Inicial : Posicion_Final;
        }
    }
}
