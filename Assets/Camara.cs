﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour {

    public Transform personaje;
    public Vector3 desplazamiento;
    public GameObject jugador;

    void Start()
    {
        jugador = GameObject.FindWithTag("Jugador");
        desplazamiento = transform.position - jugador.transform.position;
    }

    // Update is called once per frame
    void FixedUpdate () {

        //transform.position = new Vector3(personaje.position.x + desplazamiento.x, personaje.position.y + desplazamiento.y, desplazamiento.z);
	}
    void LateUpdate()
    {
        follower();
    }


    void follower()
    {
        jugador = GameObject.FindWithTag("Jugador");
        transform.position = jugador.transform.position + desplazamiento;
    }

}

